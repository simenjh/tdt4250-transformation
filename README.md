

I have set up the classes I have deemed necessary to model the course website. 

I was not able to assign relations to the different classes. E.g. I was not able to associate an instance of courseInstance with an instance of department via the belongsTo relation.

Regarding constraints I wasn't entirely sure about the syntax for the OCL expressions. Therefore, I have not finished the OCL statement for checking if the evaluation activities weight fractions sum to 1.